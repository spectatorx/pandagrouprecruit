<?php include "header.php" ?>

<ul id="headlines">

    <?php foreach ($results['news'] as $news) { ?>

        <li>
            <h2>
                <span class="created_at"><?php echo date('j F', $news->created_at) ?></span><a href=".?action=viewNews&amp;newsId=<?php echo $news->id ?>"><?php echo htmlspecialchars($news->name) ?></a>
            </h2>
            <p class="description"><?php echo htmlspecialchars($news->description) ?></p>
        </li>

    <?php } ?>

</ul>

<p><a href="./?action=archive">News Archive</a></p>

<?php include "footer.php" ?>