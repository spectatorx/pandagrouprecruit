<?php include "header.php" ?>

<div id="adminHeader">
    <h2>Admin</h2>
    <p>You are logged in as <b><?php echo htmlspecialchars($_SESSION['username']) ?></b>. <a href="admin.php?action=logout">Log out</a></p>
</div>

<h1>All news</h1>

<?php if (isset($results['errorMessage'])) { ?>
    <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>


<?php if (isset($results['statusMessage'])) { ?>
    <div class="statusMessage"><?php echo $results['statusMessage'] ?></div>
<?php } ?>

<table>
    <tr>
        <th>Publication Date</th>
        <th>News</th>
    </tr>

    <?php foreach ($results['news'] as $news) { ?>

        <tr onclick="location = 'admin.php?action=editNews&amp;newsId=<?php echo $news->id ?>'">
            <td><?php echo date('j M Y', $news->created_at) ?></td>
            <td>
                <?php echo $news->name ?>
            </td>
        </tr>

    <?php } ?>

</table>

<p><?php echo $results['totalRows'] ?> news<?php echo ( $results['totalRows'] != 1 ) ? 's' : '' ?> in total.</p>

<p><a href="admin.php?action=newNews">Add a new news</a></p>

<?php include "footer.php" ?>