<?php include "header.php" ?>

<h1>News Archive</h1>

<ul id="headlines" class="archive">

    <?php foreach ($results['news'] as $news) { ?>

        <li>
            <h2>
                <span class="created_at"><?php echo date('j F Y', $news->created_at) ?></span><a href=".?action=viewNews&amp;newsId=<?php echo $news->id ?>"><?php echo htmlspecialchars($news->name) ?></a>
            </h2>
            <p class="summary"><?php echo htmlspecialchars($news->description) ?></p>
        </li>

    <?php } ?>

</ul>

<p><?php echo $results['totalRows'] ?> news<?php echo ( $results['totalRows'] != 1 ) ? 's' : '' ?> in total.</p>

<p><a href="./">Return to Homepage</a></p>

<?php include "footer.php" ?>