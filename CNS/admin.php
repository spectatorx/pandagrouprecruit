<?php

include('db_conn.php');
session_start();
$action = isset($_GET['action']) ? $_GET['action'] : "";
$username = isset($_SESSION['username']) ? $_SESSION['username'] : "";

if ($action != "login" && $action != "logout" && !$username) {
    login();
    exit;
}

switch ($action) {
    case 'login':
        login();
        break;
    case 'logout':
        logout();
        break;
    case 'newNews':
        newNews();
        break;
    case 'editNews':
        editNews();
        break;
    case 'deleteNews':
        deleteNews();
        break;
    default:
        listNews();
}

function login() {

    $results = array();
    $results['pageTitle'] = "Admin Login | Lame News System";

    if (isset($_POST['login'])) {

        if ($_POST['username'] == ADMIN_USERNAME && $_POST['password'] == ADMIN_PASSWORD) {

            $_SESSION['username'] = ADMIN_USERNAME;
            header("Location: admin.php");
        } else {
            $results['errorMessage'] = "Incorrect username or password. Please try again.";
            require( TEMPLATE_PATH . "loginForm.php" );
        }
    } else {

        require( TEMPLATE_PATH . "loginForm.php" );
    }
}

function logout() {
    unset($_SESSION['username']);
    header("Location: admin.php");
}

function newNews() {

    $results = array();
    $results['pageTitle'] = "New News";
    $results['formAction'] = "newNews";

    if (isset($_POST['saveChanges'])) {

        $news = new News;
        $news->storeFormValues($_POST);
        $news->insert();
        header("Location: admin.php?status=changesSaved");
    } elseif (isset($_POST['cancel'])) {

        header("Location: admin.php");
    } else {

        $results['news'] = new News;
        require( TEMPLATE_PATH . "editNews.php" );
    }
}

function editNews() {

    $results = array();
    $results['pageTitle'] = "Edit News";
    $results['formAction'] = "editNews";

    if (isset($_POST['saveChanges'])) {


        if (!$news = News::getById((int) $_POST['newsId'])) {
            header("Location: admin.php?error=newsNotFound");
            return;
        }

        $news->storeFormValues($_POST);
        $news->update();
        header("Location: admin.php?status=changesSaved");
    } elseif (isset($_POST['cancel'])) {

        header("Location: admin.php");
    } else {

        $results['news'] = News::getById((int) $_GET['newsId']);
        require( TEMPLATE_PATH . "editNews.php" );
    }
}

function deleteNews() {

    if (!$news = News::getById((int) $_GET['newsId'])) {
        header("Location: admin.php?error=newsNotFound");
        return;
    }

    $news->delete();
    header("Location: admin.php?status=newsDeleted");
}

function listNewss() {
    $results = array();
    $data = News::getList();
    $results['newss'] = $data['results'];
    $results['totalRows'] = $data['totalRows'];
    $results['pageTitle'] = "All Newss";

    if (isset($_GET['error'])) {
        if ($_GET['error'] == "newsNotFound") {
            $results['errorMessage'] = "Error: News not found.";
        }
    }

    if (isset($_GET['status'])) {
        if ($_GET['status'] == "changesSaved") {
            $results['statusMessage'] = "Your changes have been saved.";
        }
        if ($_GET['status'] == "newsDeleted") {
            $results['statusMessage'] = "News deleted.";
        }
    }

    require( TEMPLATE_PATH . "listNewss.php" );
}
?>