<?php include "header.php" ?>

<div id="adminHeader">
    <h2>Admin</h2>
    <p>You are logged in as <b><?php echo htmlspecialchars($_SESSION['username']) ?></b>. <a href="admin.php?action=logout"?>Log out</a></p>
</div>

<h1><?php echo $results['pageTitle'] ?></h1>

<form action="admin.php?action=<?php echo $results['formAction'] ?>" method="post">
    <input type="hidden" name="newsId" value="<?php echo $results['news']->id ?>"/>

    <?php if (isset($results['errorMessage'])) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
    <?php } ?>

    <ul>

        <li>
            <label for="title">News name</label>
            <input type="text" name="title" id="title" placeholder="Name of the news" required autofocus maxlength="255" value="<?php echo htmlspecialchars($results['news']->name) ?>" />
        </li>

        <li>
            <label for="summary">News description</label>
            <textarea name="summary" id="summary" placeholder="Brief description of the news" required maxlength="1000" style="height: 5em;"><?php echo htmlspecialchars($results['news']->description) ?></textarea>
        </li>

        <li>
            <label for="publicationDate">Publication Date</label>
            <input type="date" name="publicationDate" id="publicationDate" placeholder="YYYY-MM-DD" required maxlength="10" value="<?php echo $results['news']->created_at ? date("Y-m-d", $results['news']->created_at) : "" ?>" />
        </li>


    </ul>

    <div class="buttons">
        <input type="submit" name="saveChanges" value="Save Changes" />
        <input type="submit" formnovalidate name="cancel" value="Cancel" />
    </div>

</form>

<?php if ($results['news']->id) { ?>
    <p><a href="admin.php?action=deleteNews&amp;newsId=<?php echo $results['news']->id ?>" onclick="return confirm('Delete This News?')">Delete This News</a></p>
<?php } ?>

<?php include "footer.php" ?>