<?php include "header.php" ?>

<h1><?php echo htmlspecialchars($results['news']->name) ?></h1>
<div><?php echo htmlspecialchars($results['news']->description) ?></div>
<div><?php echo $results['news']->content ?></div>
<p class="created_at">Published on <?php echo date('j F Y', $results['news']->created_at) ?></p>

<p><a href="./">Return to Homepage</a></p>

<?php include "footer.php" ?>