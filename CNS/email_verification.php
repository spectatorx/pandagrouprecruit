<?php
include('db_conn.php');

$message = '';

if (isset($_GET['activation_code'])) {
    $query = "
  SELECT * FROM users
  WHERE activation_code = :activation_code
 ";
    $statement = $connect->prepare($query);
    $statement->execute(
            array(
                ':activation_code' => $_GET['activation_code']
            )
    );
    $no_of_row = $statement->rowCount();

    if ($no_of_row > 0) {
        $result = $statement->fetchAll();
        foreach ($result as $row) {
            if ($row['is_active'] == 'not verified') {
                $update_query = "
    UPDATE register_user 
    SET is_active = '1' 
    WHERE register_id = '" . $row['register_id'] . "'
    ";
                $statement = $connect->prepare($update_query);
                $statement->execute();
                $sub_result = $statement->fetchAll();
                if (isset($sub_result)) {
                    $message = '<label>Your Email Address Successfully Verified <br />You can login here - <a href="login.php">Login</a></label>';
                }
            } else {
                $message = '<label>Your Email Address Already Verified</label>';
            }
        }
    } else {
        $message = '<label>Invalid Link</label>';
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>  
    </head>
    <body>

        <div>
            <h1>Verification</h1>

            <h3><?php echo $message; ?></h3>

        </div>

    </body>

</html>