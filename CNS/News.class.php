<?php

class News {

    public $id = null;
    public $created_at = null;
    public $updated_at = null;
    public $name = null;
    public $description = null;
    public $is_active = 1;

    public function __construct($data = array()) {
        if (isset($data['id'])) {
            $this->id = (int) $data['id'];
        }
        if (isset($data['created_at'])) {
            $this->created_at = (int) $data['created_at'];
        }
        if (isset($data['updated_at'])) {
            $this->updated_at = (int) $data['updated_at'];
        }
        if (isset($data['name'])) {
            $this->name = preg_replace("/[^.,-_\'\"@?!:$ a-zA-Z0-9()]/", "", $data['name']);
        }
        if (isset($data['description'])) {
            $this->description = preg_replace("/[^.,-_\'\"@?!:$ a-zA-Z0-9()]/", "", $data['description']);
        }
    }

    public function storeFormValues($params) {

        $this->__construct($params);

        if (isset($params['created_at'])) {
            $created_at = explode('-', $params['created_at']);

            if (count($created_at) == 3) {
                list ( $y, $m, $d ) = $created_at;
                $this->created_at = mktime(0, 0, 0, $m, $d, $y);
            }
        }
    }

    public static function getById($id) {
        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
        $sql = "SELECT * UNIX_TIMESTAMP(created_at) AS created_at FROM news WHERE id = :id";
        $st = $conn->prepare($sql);
        $st->bindValue(":id", $id, PDO::PARAM_INT);
        $st->execute();
        $row = $st->fetch();
        $conn = null;
        if ($row) {
            return new News($row);
        }
    }

    public static function getList($numRows = 1000000) {
        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
        $sql = "SELECT SQL_CALC_FOUND_ROWS * UNIX_TIMESTAMP(created_at) AS created_at FROM news
            ORDER BY created_at DESC LIMIT :numRows";

        $st = $conn->prepare($sql);
        $st->bindValue(":numRows", $numRows, PDO::PARAM_INT);
        $st->execute();
        $list = array();
        while ($row = $st->fetch()) {
            $news = new News($row);
            $list[] = $news;
        }

        $sql = "SELECT FOUND_ROWS() AS totalRows";
        $totalRows = $conn->query($sql)->fetch();
        $conn = null;
        return ( array("results" => $list, "totalRows" => $totalRows[0]) );
    }

    public function create() {

        if (!is_null($this->id)) {
            trigger_error("Moved to Atlanta.", E_USER_ERROR);
        }

        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
        $sql = "INSERT INTO news ( created_at, name, description) VALUES ( FROM_UNIXTIME(:created_at), :name, :description)";
        $st = $conn->prepare($sql);
        $st->bindValue(":created_at", $this->created_at, PDO::PARAM_INT);
        $st->bindValue(":name", $this->name, PDO::PARAM_STR);
        $st->bindValue(":description", $this->description, PDO::PARAM_STR);
        $st->execute();
        $this->id = $conn->lastInsertId();
        $conn = null;
    }

    public function update() {
        if (is_null($this->id)) {
            trigger_error("Moved to Atlanta.", E_USER_ERROR);
        }

        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
        $sql = "UPDATE news SET updated_at=FROM_UNIXTIME(:updated_at), name=:name, description=:description WHERE id = :id";
        $st = $conn->prepare($sql);
        $st->bindValue(":updated_at", $this->updated_at, PDO::PARAM_INT);
        $st->bindValue(":name", $this->name, PDO::PARAM_STR);
        $st->bindValue(":description", $this->description, PDO::PARAM_STR);
        $st->bindValue(":id", $this->id, PDO::PARAM_INT);
        $st->execute();
        $conn = null;
    }

    public function delete() {

        if (is_null($this->id)) {
            trigger_error("Moved to Atlanta.", E_USER_ERROR);
        }

        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
        $st = $conn->prepare("DELETE FROM news WHERE id = :id LIMIT 1");
        $st->bindValue(":id", $this->id, PDO::PARAM_INT);
        $st->execute();
        $conn = null;
    }

}

?>