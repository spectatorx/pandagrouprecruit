<?php
include('db_conn.php');

if (isset($_SESSION['id'])) {
    header("location:index.php");
}

$message = '';

if (isset($_POST["register"])) {
    $query = "
 SELECT * FROM users 
 WHERE email = :email
 ";
    $statement = $connect->prepare($query);
    $statement->execute(
            array(
                ':email' => $_POST['email']
            )
    );
    $no_of_row = $statement->rowCount();
    if ($no_of_row > 0) {
        $message = '<label>Email Already Exists</label>';
    } else {
        $password = rand(100000, 999999);
        $encrypted_password = password_hash($password, PASSWORD_ARGON2ID);
        $activation_code = md5(rand());
        $insert_query = "
  INSERT INTO users
  (name, email, password, activation_code, email_status) 
  VALUES (:name, :email, :password, :activation_code, :is_active)
  ";
        $statement = $connect->prepare($insert_query);
        $statement->execute(
                array(
                    ':name' => $_POST['name'],
                    ':email' => $_POST['email'],
                    ':password' => $encrypted_password,
                    ':activation_code' => $activation_code,
                    ':is_active' => '0'
                )
        );
        $result = $statement->fetchAll();
        if (isset($result)) {
            $base_url = "http://localhost/";
            $mail_body = "
   <p>Hi " . $_POST['name'] . ",</p>
   <p>Thanks for Registration. Your password is " . $password . ", This password will work only after your email verification.</p>
   <p>Please Open this link to verify your email address - " . $base_url . "email_verification.php?activation_code=" . $activation_code . "
   ";
            require 'PHPMailer.php';
            $mail = new PHPMailer;
            $mail->IsSMTP();
            $mail->Host = 'smtpout.secureserver.net';
            $mail->Port = '80';
            $mail->SMTPAuth = true;
            $mail->Username = '';
            $mail->Password = '';
            $mail->SMTPSecure = '';
            $mail->From = 'mail@mail.com';
            $mail->FromName = 'Your friendly admin';
            $mail->AddAddress($_POST['email'], $_POST['name']);
            $mail->WordWrap = 50;
            $mail->IsHTML(true);
            $mail->Subject = 'Email Verification';
            $mail->Body = $mail_body;
            if ($mail->Send()) {
                $message = '<label>Registration done, verification email sent.</label>';
            }
        }
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title></title>  
    </head>
    <body>
        <br />
        <div>
            <h2>Register</h2>
            <br />
            <div>
                <div><h4>Register</h4></div>
                <div>
                    <form method="post" id="register_form">
<?php echo $message; ?>
                        <div>
                            <label>User Name</label>
                            <input type="text" name="name" pattern="[a-zA-Z ]+" required />
                        </div>
                        <div>
                            <label>User Email</label>
                            <input type="email" name="email" required />
                        </div>
                        <div>
                            <input type="submit" name="register" id="register" value="Register"/>
                        </div>
                    </form>
                    <p><a href="login.php">Login</a></p>
                </div>
            </div>
        </div>
    </body>
</html>
