<?php

include('db_conn.php');
$action = isset($_GET['action']) ? $_GET['action'] : "";

switch ($action) {
    case 'archive':
        archive();
        break;
    case 'viewNews':
        viewNews();
        break;
    default:
        homepage();
}

function archive() {
    $results = array();
    $data = News::getList();
    $results['news'] = $data['results'];
    $results['totalRows'] = $data['totalRows'];
    $results['pageTitle'] = "News Archive | Lame News System";
    require( TEMPLATE_PATH . "/archive.php" );
}

function viewNews() {
    if (!isset($_GET["newsId"]) || !$_GET["newsId"]) {
        homepage();
        return;
    }

    $results = array();
    $results['news'] = News::getById((int) $_GET["newsId"]);
    $results['pageTitle'] = $results['news']->title . " | Lame News System";
    require( TEMPLATE_PATH . "/viewNews.php" );
}

function homepage() {
    $results = array();
    $data = News::getList(HOMEPAGE_NUM_ARTICLES);
    $results['news'] = $data['results'];
    $results['totalRows'] = $data['totalRows'];
    $results['pageTitle'] = "Lame News System";
    require( TEMPLATE_PATH . "/homepage.php" );
}

?>