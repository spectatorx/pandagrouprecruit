<?php
include('db_conn.php');

if (isset($_SESSION['id'])) {
    header("location:index.php");
}

$message = '';

if (isset($_POST["login"])) {
    $query = "
 SELECT * FROM users 
  WHERE email = :email
 ";
    $statement = $connect->prepare($query);
    $statement->execute(
            array(
                'email' => $_POST["email"]
            )
    );
    $count = $statement->rowCount();
    if ($count > 0) {
        $result = $statement->fetchAll();
        foreach ($result as $row) {
            if ($row['is_active'] == '1') {
                if (password_verify($_POST["password"], $row["password"])) {
                    $_SESSION['id'] = $row['register_id'];
                    header("location:index.php");
                } else {
                    $message = "<label>Wrong password</label>";
                }
            } else {
                $message = "<label>Verify your email address first.</label>";
            }
        }
    } else {
        $message = "<label>Wrong email address</label>";
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title></title>  
    </head>
    <body>
        <br />
        <div>
            <h2>Login</h2>
            <br />
            <div>
                <div><h4>Login</h4></div>
                <div>
                    <form method="post">
                        <?php echo $message; ?>
                        <div>
                            <label>User Email</label>
                            <input type="email" name="email" required />
                        </div>
                        <div>
                            <label>Password</label>
                            <input type="password" name="password" required />
                        </div>
                        <div>
                            <input type="submit" name="login" value="Login" />
                        </div>
                    </form>
                    <p><a href="register.php">Register</a></p>
                </div>
            </div>
        </div>
    </body>
</html>
