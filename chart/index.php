<?php
include('db_conn.php');

$filename = "mock_data.csv";

if (!file_exists($filename)) {
    die("File not found. Make sure you have here file named mock_data.csv.");
}

$query = <<<eof
    LOAD DATA INFILE '$fileName'
     INTO TABLE chart_users
     FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
     LINES TERMINATED BY '\n'
    (id,first_name,last_name,email,gender,country)
eof;

$connect->query($query);

$country = "
 SELECT * FROM chart_users 
 WHERE country = :country
 ";
$statement = $connect->prepare($country);
$statement->execute(
        array(
            ':country' => $_POST['country']
        )
);

$data = array();
foreach ($result as $row) {
    $data[] = $row;
}

print json_encode($data);
?>
<html>
    <head>
        <title>CSV chart</title>
        <style type="text/css">
            #chart-container {
                width: 640px;
                height: auto;
            }
        </style>
    </head>
    <body>
        <div id="chart-container">
            <canvas id="country_chart"></canvas>
        </div>        
        <script type="text/javascript" src="js/app.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js" integrity="sha256-Uv9BNBucvCPipKQ2NS9wYpJmi8DTOEfTA/nH2aoJALw=" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    </body>
</html>