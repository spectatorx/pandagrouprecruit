$(document).ready(function () {
    $.ajax({
        url: "http://localhost/index.php",
        method: "GET",
        success: function (data) {
            console.log(data);
            var country = [];

            for (var i in data) {
                country.push("Country " + data[i].countryid);
            }

            var chartdata = {
                labels: country,
                datasets: [
                    {
                        label: 'Country',
                        backgroundColor: 'rgba(200, 200, 200, 0.75)',
                        borderColor: 'rgba(200, 200, 200, 0.75)',
                        hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
                        hoverBorderColor: 'rgba(200, 200, 200, 1)',
                        data: score
                    }
                ]
            };

            var ctx = $("#country_chart");

            var barGraph = new Chart(ctx, {
                type: 'bar',
                data: chartdata
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
});